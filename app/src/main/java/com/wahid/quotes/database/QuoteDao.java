package com.wahid.quotes.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.wahid.quotes.models.Quote;

@Dao
public abstract class QuoteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract void insertInternal(Quote quote);
    /*
        - delete all old values before we insert the new record. We make it in one transaction to reduce DB connections.
        - if we want to show the quotes history, we can remove the deleteAll(), and add a date field to the quote model.
     */
    @Transaction
    public void insert(Quote quote){
        deleteAll();
        insertInternal(quote);
    }

    @Query("SELECT * FROM Quote LIMIT 1")
    public abstract Quote getLastQuote();

    @Query("DELETE FROM Quote")
    abstract void deleteAll();
}
