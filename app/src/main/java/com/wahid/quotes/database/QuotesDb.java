package com.wahid.quotes.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.wahid.quotes.models.Quote;

@Database( entities = {Quote.class}, version = 1, exportSchema = false)
public abstract class QuotesDb extends RoomDatabase {
    private static QuotesDb instance;
    private static final String DATABASE_NAME = "quotes.db";

    public abstract QuoteDao quoteDao();
    private static final Object lock = new Object();

    public static QuotesDb getInstance(Context context) {
        synchronized (lock) {
            if (instance == null) {
                instance = Room.databaseBuilder(context, QuotesDb.class, DATABASE_NAME)
                        .allowMainThreadQueries()
                        .build();
            }
        }
        return instance;
    }
}
