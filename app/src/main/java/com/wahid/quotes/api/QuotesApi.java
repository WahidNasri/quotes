package com.wahid.quotes.api;

import com.wahid.quotes.callbacks.GenericCallback;
import com.wahid.quotes.models.Quote;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;
import timber.log.Timber;

public class QuotesApi {
    private static Quotes getClient(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://programming-quotes-api.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getLoggerClient())
                .build();

        return retrofit.create(Quotes.class);
    }
    private static OkHttpClient getLoggerClient(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message -> Timber.tag("##API##").i(message));
        logging.level(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
    }

    public static void getRandomQuotes(final GenericCallback<Quote> quoteCallback){
        getClient().getRandomQuote().enqueue(new Callback<Quote>() {
            @Override
            public void onResponse(Call<Quote> call, Response<Quote> response) {
                if (response.isSuccessful()){
                    quoteCallback.result(response.body());
                }
                else {
                    quoteCallback.result(null);
                }
            }

            @Override
            public void onFailure(Call<Quote> call, Throwable t) {
                t.printStackTrace();
                quoteCallback.result(null);
            }
        });
    }
}
