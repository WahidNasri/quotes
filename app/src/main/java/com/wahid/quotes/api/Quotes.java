package com.wahid.quotes.api;

import com.wahid.quotes.models.Quote;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Quotes {
    @GET("quotes/random")
    Call<Quote> getRandomQuote();
}
