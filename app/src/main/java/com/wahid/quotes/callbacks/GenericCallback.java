package com.wahid.quotes.callbacks;

public interface GenericCallback<T> {
    void result(T t);
}
