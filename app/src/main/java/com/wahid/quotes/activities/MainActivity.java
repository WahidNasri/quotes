package com.wahid.quotes.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.wahid.quotes.R;
import com.wahid.quotes.api.QuotesApi;
import com.wahid.quotes.utils.DataProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.quote) TextView quote;
    @BindView(R.id.author) TextView author;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.loading) ProgressBar loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //start load quote on startup
        loadQuote(true);
    }
    private void loadQuote(boolean startup){
        fab.setEnabled(false);
        loading.setVisibility(View.VISIBLE);

        DataProvider.loadQuote(startup, this, randomQuote -> {
            fab.setEnabled(true);
            loading.setVisibility(View.GONE);

            if (randomQuote != null) {
                quote.setText(randomQuote.getText());
                author.setText(randomQuote.getAuthor());
            }
            else {
                Toast.makeText(this, R.string.error_loading_quote, Toast.LENGTH_LONG).show();
            }
        });
    }
    @OnClick(R.id.fab)
    void onFabClicked(){
        loadQuote(false);
    }
}
