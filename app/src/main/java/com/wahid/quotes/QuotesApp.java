package com.wahid.quotes;

import android.app.Application;

import com.wahid.quotes.BuildConfig;

import timber.log.Timber;

public class QuotesApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
