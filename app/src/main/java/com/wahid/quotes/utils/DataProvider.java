package com.wahid.quotes.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.wahid.quotes.api.QuotesApi;
import com.wahid.quotes.callbacks.GenericCallback;
import com.wahid.quotes.database.QuotesDb;
import com.wahid.quotes.models.Quote;

public class DataProvider {
    public static void loadQuote(boolean startup, Context context, GenericCallback<Quote> quoteCallback){
        if (!isNetworkAvailable(context)){
            if (startup){
                quoteCallback.result(QuotesDb.getInstance(context).quoteDao().getLastQuote());
            }
            else {
                quoteCallback.result(null);
            }
        }
        else {
            if (!isNetworkAvailable(context)){
                quoteCallback.result(null);
            }
            else {
                QuotesApi.getRandomQuotes(quote -> {
                    if (quote != null) {
                        QuotesDb.getInstance(context).quoteDao().insert(quote);
                    }
                    quoteCallback.result(quote);
                });
            }
        }
    }
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
